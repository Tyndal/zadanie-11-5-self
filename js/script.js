$(function() {
  function Button(text) {
    this.text = text || "Hello";
  }

  // $ - wybór danego elementu

  Button.prototype.create = function() {
    //metoda przypisywana do funkcji
    let self = this;
    this.$element = $("<button>");
    this.$element.text(this.text);
    this.$element.click(function() {
      alert(self.text);
    });
    $("body").append(this.$element);
  };

  let btn1 = new Button("raz dwa trzy");
  btn1.create();
});
